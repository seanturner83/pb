# README #

[![Deploy to Azure](http://azuredeploy.net/deploybutton.png)](https://azuredeploy.net/)

### What is this repository for? ###

Creates:

* An app service plan
* Two web applications:
    * A front-end application (dave-frontend)
    * An API application (dave-api)
* A blank SQL database called dave-sql

### How do I get set up? ###

One of:

1. (On the web - bit scary) Click the button above and change the deployment settings (resource group name) - *note if you want to specify the location to deploy to using this method, you'll have to manually create the resource group in the right location first - I'm not sure why this option is missing*
2. (Windows) Install Azure PowerShell - https://azure.microsoft.com/en-gb/documentation/articles/powershell-install-configure/ - check out the code, switch to the checked out directory and run deploy.ps1
3. (Cross-platform) Install Azure Cross Platform CLI - https://azure.microsoft.com/en-gb/documentation/articles/xplat-cli-install/ - check out the code, switch to the checked out directory and run deploy.sh (supply parameters for sub id, rg name, name, location)

### Who do I talk to? ###

* seanturner83@gmail.com