﻿function Check-Session () {
    $Error.Clear()

    #if context already exist
    Get-AzureRmContext -ErrorAction Continue
    foreach ($eacherror in $Error) {
        if ($eacherror.Exception.ToString() -like "*Run Login-AzureRmAccount to login.*") {
            Login-AzureRmAccount
        }
    }

    $Error.Clear();
}

#check if session exists, if not then prompt for login
Check-Session

New-AzureRmResourceGroup -Name Dave -Location "West Europe"
New-AzureRmResourceGroupDeployment -ResourceGroupName Dave -TemplateFile "azuredeploy.json"
